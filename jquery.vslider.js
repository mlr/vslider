/*
* Vertical content slider
* Copyright 2011, Ronnie Miller
* Released under the MIT license
* http://ronniemlr.com/jquery/vslider/
*/

(function($) {
$.fn.vSlider = function(options) {
	var defaults = {
		show: 3,
		height: 0,		
		speed: 200,
		pause: 5000,
		wrap: true,
		autoStart: false,
		createButtons: true,
		buttons: {
			wrapper: 'vslider_buttons',
			up: 'vslider_up',
			down: 'vslider_down'
		}
	};
	
	var options = $.extend(defaults, options);
	var current_bottom = options.show;
	var total_items = 0;
	var wait = false;
	
	slide = function(container, direction) {
		if(wait || (!options.wrap && direction == 'up' && current_bottom == options.show) || (!options.wrap && direction == 'down' && current_bottom == total_items))
			return;

		wait = true;
		current_bottom += (direction == 'up') ? -1 : 1;
		move = (direction == 'up') ? options.height : options.height * -1;
		
		// check if we need to wrap
		if(options.wrap && (current_bottom == options.show-1 && direction == 'up')) {
			move -= options.height * (total_items - options.show + 1);
			current_bottom = total_items;
		}
		
		if(options.wrap && (current_bottom-1 == total_items && direction == 'down')) {
			move += options.height * (total_items - options.show + 1);
			current_bottom = options.show;
		}

		// do the move with animation
		container.find('li').css({ position: 'relative' }).each(function() {
			$(this).animate({ top: '+=' + move +'px' }, options.speed, '', function() { wait = false; });
		});
	};
	
	return this.each(function() {
		$container = $(this);
		$container.css({ overflow: 'hidden' });

		if(options.height == 0)
			options.height = $container.find('li').height();
			
		// Are we creating buttons for the user?
		if(options.createButtons) {
			$('<div />').addClass(options.buttons.wrapper).insertAfter($container)
				.append('<a href="#" class="'+ options.buttons.up + '">Up</a>').end()
				.append('<a href="#" class="'+ options.buttons.down + '">Down</a>').end();
		}
			
		total_items = $container.children('li').length;
		current_bottom = options.show;
		
		/*
		 * Bind click events for the up and down buttons. Each calls the slide method above.
		 * Which will calculate the amount of pixels to move and start the animation.
		*/
		$('.'+options.buttons.wrapper+' .'+options.buttons.up).bind('click', function(e) {
			e.preventDefault();
			slide($container, 'up');
		});
		
		$('.'+options.buttons.wrapper+' .'+options.buttons.down).bind('click', function(e) {
			e.preventDefault();
			slide($container, 'down');
		});
		
		// Autostart the animation?
		if(options.autoStart) {
			window.setInterval(function() { slide($container, 'down'); }, options.pause);
		}
	});
};
})(jQuery);